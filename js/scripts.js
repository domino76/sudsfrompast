$(document).ready(function () {
    $('#vendorTable').DataTable({
        "bLengthChange": false,
        "ordering": false,
        "iDisplayLength": 6,
        "pagingType": "simple",
        "info": false,
        language: {
            searchPlaceholder: "Type to filter list"
        }
    });
    
    $('#drinkTable').DataTable({
        "bLengthChange": false,
        "ordering": true,
        "iDisplayLength": 6,
        "pagingType": "simple",
        "info": false,
        language: {
            searchPlaceholder: "Type to filter list"
        }
    });
    
//    $('#wishlistTable').DataTable({
//        "bLengthChange": false,
//        "ordering": false,
//        "iDisplayLength": 6,
//        "pagingType": "simple",
//        "info": false
//    });
    
});

function VendorViewModel() {
    var self = this;

    self.logo = ko.observable("images/sudsLogo.jpg");
    self.details = ko.observable("Welcome to Suds on the Shore 2017");
    self.vendorName = ko.observable("Suds on the Shore");
    self.vendorNameOther = ko.observable();
    self.vendorLocation = ko.observable("Ludington, MI");
    self.menuList = ko.observable();
    self.vendorList = ko.observable();
    self.drinkList = ko.observable();
    self.showButton = ko.observable(false);
    self.isActive = ko.observable();
    self.myWishlist = ko.observable();
    self.addToWishlist = ko.observable();
    self.removeFromList = ko.observable();
    self.list = ko.observable();
    self.storedList = ko.observable();
    
    self.addToWishlist = function(itemToAdd){
        // Check if wishlist key exists, create if it does not
        if (localStorage.getItem("wishlist") === null) {
            var wishlist = [];
            wishlist.push(itemToAdd);
            localStorage.setItem('wishlist', JSON.stringify(wishlist));
            alert("Drink added");
        } else {
            var currentList = JSON.parse(localStorage.getItem('wishlist'));
            var convertToValue = Object.values(itemToAdd);
            var drinkName = convertToValue[0];
            var count = 0;
            for (var i = 0; i < currentList.length; i++){               
                var currName =  currentList[i].name;
                
                if (currName === drinkName){                                 
                    count = count + 1;  
                }
            }
            
            if ( count === 0){
                currentList.push(itemToAdd);
                alert("Drink added");
            } else {
                alert("Already on Wishlist");
            }
            
            localStorage.setItem('wishlist', JSON.stringify(currentList)); 
        }
    }
  
    self.removeFromList = function(itemToDelete){
        var currentList = JSON.parse(localStorage.getItem('wishlist'));  
        var convertToValue = Object.values(itemToDelete);
        var drinkName = convertToValue[0];
        
        for (var i = 0; i < currentList.length; i++){               
            var currName =  currentList[i].name;         
            if (currName === drinkName){                                 
                currentList.splice(i, 1);
            } 
        }
        localStorage.setItem('wishlist', JSON.stringify(currentList)); 
        
        $("#wishlistModal").modal("hide");
        
    }
      
    self.myWishlist = function() {
        self.storedList(JSON.parse(localStorage.getItem('wishlist')));    
    }
        
    self.angryOrchard = function () {
    self.logo("images/angryOrchardCider.jpeg");
    self.details("Angry Orchard");
    self.vendorName("Angry Orchard Cider Co");
    self.vendorNameOther("Traveler Beer Co");
    self.vendorLocation("");
    self.showButton(true);
    self.isActive("angryOrchard");
    self.menuList([{
            name: "Porter Name",
            type: "Porter",
            abv: "5"
            },
        {
            name: "Pilsner Name",
            type: "Pilsner",
            abv: "5"
            },
        {
            name: "Ale Name",
            type: "Golden Ale",
            abv: "5"
            }
        ]);
    };
    
    self.arbor = function () {
        self.logo("images/arbor.jpeg");
        self.details("Arbor");
        self.vendorName("Arbor Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Ypsilanti, MI");
        self.showButton(true);
        self.isActive("arbor");
        self.menuList([{
                name: "Expresso Love Breakfast Stout",
                type: "Stout",
                abv: "6.7",
                vendor: "Arbor Brewing"
                },
            {
                name: "Strawberry Blonde",
                type: "Blonde",
                abv: "5",
                vendor: "Arbor Brewing"
                },
            {
                name: "Sacred Cow",
                type: "IPA",
                abv: "5",
                vendor: "Arbor Brewing"
                }
            ]);
    };
    
    self.arcadia = function () {
        self.logo("images/arcadia.jpeg");
        self.details("Arcadia");
        self.vendorName("Arcadia Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Kalamazoo, MI");
        self.showButton(true);
        self.isActive("arcadia");
        self.menuList([{
                name: "Whitsun",
                type: "American Wheat Ale",
                vendor: "Arcadia Brewing",
                abv: "6"
                },
            {
                name: "Cheap Date",
                type: "IPA - Session",
                abv: "4.5",
                vendor: "Arcadia Brewing"
                },
            {
                name: "Hopmouth",
                type: "IPA - Double",
                abv: "8",
                vendor: "Arcadia Brewing"
                },
            {
                name: "MI-Berry Vice",
                type: "Fruit",
                abv: "6.3",
                vendor: "Arcadia Brewing"
                }
            ]);
    };

    self.atwater = function () {
        self.logo("images/atwater.jpeg");
        self.details("Atwater");
        self.vendorName("Atwater Brewery");
        self.vendorNameOther("");
        self.vendorLocation("Detroit, MI");
        self.showButton(true);
        self.isActive("atwater");
        self.menuList([{
                name: "Dirty Blonde",
                type: "Wheat Ale",
                abv: "5",
                vendor: "Arcadia Brewing"
                },
            {
                name: "Vanilla Java Porter",
                type: "Porter",
                abv: "5",
                vendor: "Arcadia Brewing"
                },
            {
                name: "Purple Gang",
                type: "German Pilsner",
                abv: "5",
                vendor: "Arcadia Brewing"
                },
            {
                name: "Blueberry Cobbler",
                type: "Porter",
                abv: "5",
                vendor: "Arcadia Brewing"
                }
            ]);
    };
    
    self.austinBros = function () {
        self.logo("images/austinBros.jpeg");
        self.details("Austin Brothers");
        self.vendorName("Austin Brothers");
        self.vendorNameOther("");
        self.vendorLocation("Alpena, MI");
        self.showButton(true);
        self.isActive("austinBros");
        self.menuList([{
                name: "Cherry Bomb",
                type: "Wheat Ale",
                abv: "5",
                vendor: "Austin Brothers"
                },
            {
                name: "Milk Route",
                type: "Stout",
                abv: "5",
                vendor: "Austin Brothers"
                },
            {
                name: "Woody Wheat",
                type: "Wheat",
                abv: "5",
                vendor: "Austin Brothers"
                },
            {
                name: "Amber Ale",
                type: "Amber",
                abv: "5",
                vendor: "Austin Brothers"
                }
            ]);
    };
    
    self.backwoods = function () {
        self.logo("images/wineglass.jpeg");
        self.details("Backwoods");
        self.vendorName("Backwoods Homemade Wines");
        self.vendorNameOther("");
        self.vendorLocation("Irons, MI");
        self.showButton(true);
            self.isActive("backwoods");
        self.menuList([{
                name: "Black Cherry",
                type: "Wine",
                abv: "5",
                vendor: "Backwoods Homemade Wines"
                },
            {
                name: "Strawberry Kiwi",
                type: "Wine",
                abv: "5",
                vendor: "Backwoods Homemade Wines"
                },
            {
                name: "Pear",
                type: "Wine",
                abv: "5",
                vendor: "Backwoods Homemade Wines"
                }
            ]);
    };
    
    self.bells = function () {
        self.logo("images/bells.jpeg");
        self.details("Bells");
        self.vendorName("Bell's Brewery");
        self.vendorNameOther("");
        self.vendorLocation("Galesburg, MI");
        self.showButton(true);
        self.isActive("bells");
        self.menuList([{
                name: "Smitten",
                type: "Pale Ale",
                abv: "5",
                vendor: "Bell's Brewery"
                },
            {
                name: "Two Hearted Ale",
                type: "American IPA",
                abv: "5",
                vendor: "Bell's Brewery"
                },
            {
                name: "Kalamazoo Stout",
                type: "Stout",
                abv: "5",
                vendor: "Bell's Brewery"
                }
            ]);
    };
    
    self.bigHart = function () {
        self.logo("images/bigHart.jpeg");
        self.details("Big Hart");
        self.vendorName("Big Hart Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Hart, MI");
        self.showButton(true);
            self.isActive("bigHart");
        self.menuList([{
                name: "Dune Rail",
                type: "American Blonde Ale",
                abv: "5",
                vendor: "Big Hart Brewing"
                },
            {
                name: "Alphonse",
                type: "Session Rye IPA",
                abv: "5",
                vendor: "Big Hart Brewing"
                },
            {
                name: "Electric Hart",
                type: "Belgian Brown",
                abv: "5",
                vendor: "Big Hart Brewing"
                }
            ]);
    };
    
    self.bigLake = function () {
        self.logo("images/bigLakeBrewing.jpeg");
        self.details("Big Lake Brewing");
        self.vendorName("Big Lake Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Holland, MI");
        self.showButton(true);
        self.isActive("bigLake");
        self.menuList([{
                name: "Pier Pressure",
                type: "IPA",
                abv: "5",
                vendor: "Big Lake Brewing"
                },
            {
                name: "Citra Session",
                type: "Session Ale",
                abv: "5",
                vendor: "Big Lake Brewing"
                },
            {
                name: "Summer Squeeze",
                type: "American Ale",
                abv: "5",
                vendor: "Big Lake Brewing"
                }
            ]);
    };
    
    self.blakes = function () {
        self.logo("images/blakes.jpeg");
        self.details("Blakes");
        self.vendorName("Blake's Hard Cider");
        self.vendorNameOther("");
        self.vendorLocation("Armada, MI");
        self.showButton(true);
        self.isActive("blakes");
        self.menuList([{
                name: "Flannel Mouth",
                type: "Cider",
                abv: "5",
                vendor: "Blake's Hard Cider"
                },
            {
                name: "El Chavo",
                type: "Cider",
                abv: "5",
                vendor: "Blake's Hard Cider"
                },
            {
                name: "Beard Bender",
                type: "Cider",
                abv: "5",
                vendor: "Blake's Hard Cider"
                }
            ]);
    };
    
    self.terraFirma = function () {
        self.logo("images/breweryTerraFirma.jpeg");
        self.details("Brewery Terra Firma");
        self.vendorName("Brewery Terra Firma");
        self.vendorNameOther("");
        self.vendorLocation("Traverse City, MI");
        self.showButton(true);
        self.isActive("terraFirma");
        self.menuList([{
                name: "Sun Cup Lemon Wheat",
                type: "Wheat",
                abv: "5",
                vendor: "Brewery Terra Firma"
                },
            {
                name: "Brown Donkey Smasher",
                type: "Brown",
                abv: "5",
                vendor: "Brewery Terra Firma"
                },
            {
                name: "Pterodact Ale",
                type: "IPA",
                abv: "5",
                vendor: "Brewery Terra Firma"
                }
            ]);
    };
        
    self.vivant = function () {
        self.logo("images/breweryVivant.jpeg");
        self.details("Vivant");
        self.vendorName("Brewery Vivant");
        self.vendorNameOther("");
        self.vendorLocation("Grand Rapids, MI");
        self.showButton(true);
        self.isActive("vivant");
        self.menuList([{
                name: "Farm Hand",
                type: "Farmhouse Ale",
                abv: "5",
                vendor: "Brewery Vivant"
                },
            {
                name: "Undertaker",
                type: "Dark Ale",
                abv: "5",
                vendor: "Brewery Vivant"
                },
            {
                name: "Big Red Coq",
                type: "Red IPA",
                abv: "5",
                vendor: "Brewery Vivant"
                }
            ]);
    };
    
    self.cascade = function () {
        self.logo("images/cascadeWinery.jpeg");
        self.details("Cascade");
        self.vendorName("Cascade Winery");
        self.vendorNameOther("Jaden James Brewery");
        self.vendorLocation("Kentwood, MI");
        self.showButton(true);
        self.isActive("cascade");
        self.menuList([{
                name: "Peanut Butter Porter",
                type: "Porter",
                abv: "5",
                vendor: "Jaden James Brewery"
                },
            {
                name: "IPA",
                type: "IPA",
                abv: "5",
                vendor: "Jaden James Brewery"
                },
            {
                name: "Black Walnut Brown Ale",
                type: "Brown",
                abv: "5",
                vendor: "Jaden James Brewery"
                }
            ]);
    };
    
    self.cheboygan = function () {
        self.logo("images/cheboyganBrewingCo.jpeg");
        self.details("Cheboygan");
        self.vendorName("Cheboygan Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Cheboygan, MI");
        self.showButton(true);
        self.isActive("cheboygan");
        self.menuList([{
                name: "Lighthouse Amber",
                type: "Amber",
                abv: "5",
                vendor: "Cheboygan Brewing"
                },
            {
                name: "Blood Orange Honey",
                type: "Wheat",
                abv: "5",
                vendor: "Cheboygan Brewing"
                },
            {
                name: "Blueberry Cream Ale",
                type: "Fruit",
                abv: "5",
                vendor: "Cheboygan Brewing"
                }
            ]);
    };
    
    self.clownShoes = function () {
        self.logo("images/clownShoes.jpeg");
        self.details("clownShoes");
        self.vendorName("Clown Shoes");
        self.vendorNameOther("");
        self.vendorLocation("Ipswich, MA");
        self.showButton(true);
        self.isActive("clownShoes");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Clown Shoes"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Clown Shoes"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Clown Shoes"
                }
            ]);
    };
    
    self.coneyIsland = function () {
        self.logo("images/coneyIsland.jpeg");
        self.details("Coney Island");
        self.vendorName("Coney Island");
        self.vendorNameOther("");
        self.vendorLocation("Brooklyn, NY");
        self.showButton(true);
        self.isActive("coneyIsland");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "xxxxxxxx"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "xxxxxxxx"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "xxxxxxxx"
                }
            ]);
    };
    
    self.crankers = function () {
        self.logo("images/crankers.jpeg");
        self.details("Crankers");
        self.vendorName("Cranker's Brewery");
        self.vendorNameOther("");
        self.vendorLocation("Big Rapids, MI");
        self.showButton(true);
        self.isActive("crankers");
        self.menuList([{
                name: "Big in Japan",
                type: "IPA - Session",
                abv: "5",
                vendor: "Cranker's Brewery"
                },
            {
                name: "Suspended Animation",
                type: "Fruit",
                abv: "5.5",
                vendor: "Cranker's Brewery"
                },
            {
                name: "Honey Kolsch",
                type: "Kolsch",
                abv: "5.2",
                vendor: "Cranker's Brewery"
                }
            ]);
    };
    
    self.darkHorse = function () {
        self.logo("images/darkHorse.jpeg");
        self.details("Dark Horse");
        self.vendorName("Dark Horse");
        self.vendorNameOther("");
        self.vendorLocation("Marshall, MI");
        self.showButton(true);
        self.isActive("darkHorse");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Dark Horse"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Dark Horse"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Dark Horse"
                }
            ]);
    };
    
    self.drStrangeLove = function () {
        self.logo("images/beerMug.jpeg");
        self.details("Dr Strange Love");
        self.vendorName("Dr Strange Love");
        self.vendorNameOther("");
        self.vendorLocation("Sweden");
        self.showButton(true);
        self.isActive("drStrangeLove");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Dr Strange Love"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Dr Strange Love"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Dr Strange Love"
                }
            ]);
    };    
    
    self.elk = function () {
        self.logo("images/elk.jpeg");
        self.details("Elk");
        self.vendorName("Elk Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Comstock Park, MI");
        self.showButton(true);
        self.isActive("elk");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Elk Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Elk Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Elk Brewing"
                }
            ]);
    };
    
    self.ellison = function () {
        self.logo("images/ellison.jpeg");
        self.details("Ellison");
        self.vendorName("Ellison Brewing");
        self.vendorNameOther("");
        self.vendorLocation("East Lansing, MI");
        self.showButton(true);
        self.isActive("ellison");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Ellison Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Ellison Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Ellison Brewing"
                }
            ]);
    };
    
    self.farmhaus = function () {
        self.logo("images/farmhaus.jpeg");
        self.details("Farmhaus");
        self.vendorName("Farmhaus Cider");
        self.vendorNameOther("");
        self.vendorLocation("Hudsonville, MI");
        self.showButton(true);
        self.isActive("farmhaus");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Farmhaus Cider"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Farmhaus Cider"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Farmhaus Cider"
                }
            ]);
    };
    
    self.fetch = function () {
        self.logo("images/fetch.jpeg");
        self.details("Fetch");
        self.vendorName("Fetch Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Whitehall, MI");
        self.showButton(true);
        self.isActive("fetch");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Fetch Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Fetch Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Fetch Brewing"
                }
            ]);
    };
    
    self.founders = function () {
        self.logo("images/founders.jpeg");
        self.details("Founders");
        self.vendorName("Founders Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Grand Rapids, MI");
        self.showButton(true);
        self.isActive("founders");
        self.menuList([{
                name: "Mosaic Promise",
                type: "IPA",
                abv: "5",
                vendor: "Founders Brewing"
                },
            {
                name: "Drink 2",
                type: "Pilsner",
                abv: "5",
                vendor: "Founders Brewing"
                },
            {
                name: "Drink 3",
                type: "Golden Ale",
                abv: "5",
                vendor: "Founders Brewing"
                }
            ]);
    };

    self.gravelBottom = function () {
        self.logo("images/gravelBottom.jpeg");
        self.details("Gravel Bottom");
        self.vendorName("Gravel Bottom Craft Brewery");
        self.vendorNameOther("");
        self.vendorLocation("Ada, MI");
        self.showButton(true);
        self.isActive("gravelBottom");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Gravel Bottom Craft Brewery"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Gravel Bottom Craft Brewery"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Gravel Bottom Craft Brewery"
                }
            ]);
    };
    
    
    
    self.greatLakesBrewing = function () {
        self.logo("images/greatLakes.jpeg");
        self.details("Great Lakes");
        self.vendorName("Great Lakes");
        self.vendorNameOther("");
        self.vendorLocation("Cleveland, OH");
        self.showButton(true);
        self.isActive("greatLakesBrewing");
        self.menuList([{
                name: "Oktoberfest",
                type: "Marzen",
                abv: "5",
                vendor: "Great Lakes"
                },
            {
                name: "Edmund Fitzgerald",
                type: "Porter",
                abv: "5",
                vendor: "Great Lakes"
                },
            {
                name: "Dortmunder Gold",
                type: "Lager",
                abv: "5",
                vendor: "Great Lakes"
                },
            {
                name: "Elliot Ness",
                type: "Amber Lager",
                abv: "5",
                vendor: "Great Lakes"
                },
            {
                name: "Burning River",
                type: "Pale Ale",
                abv: "5",
                vendor: "Great Lakes"
                }
            ]);
    };

    self.greatLakesWine = function () {
        self.logo("images/greatLakesWine.jpg");
        self.details("Great Lakes Wine");
        self.vendorName("Great Lakes Wine");
        self.vendorNameOther("");
        self.vendorLocation("Grand Rapids, MI");
        self.showButton(true);
        self.isActive("greatLakesWine");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Great Lakes Wine"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Great Lakes Wine"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Great Lakes Wine"
                }
            ]);
    };

    self.greatMeadHall = function () {
        self.logo("images/greatMeadHall.jpeg");
        self.details("Great Mead Hall");
        self.vendorName("Great Mead Hall");
        self.vendorNameOther("");
        self.vendorLocation("Bangor, MI");
        self.showButton(true);
        self.isActive("greatMeadHall");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Great Mead Hall"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Great Mead Hall"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Great Mead Hall"
                }
            ]);
    };    
    
    self.henryFox = function () {
        self.logo("images/henryFox.jpg");
        self.details("Henry Fox");
        self.vendorName("Henry Fox Sales");
        self.vendorNameOther("");
        self.vendorLocation("Grand Rapids, MI");
        self.showButton(true);
        self.isActive("henryFox");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Henry Fox Sales"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Henry Fox Sales"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Henry Fox Sales"
                }
            ]);
    };
    
    self.jamesport = function () {
        self.logo("images/jamesportBrewingCo.jpeg");
        self.details("Jamesport");
        self.vendorName("Jamesport Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Ludington, MI");
        self.showButton(true);
        self.isActive("jamesport");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Jamesport Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Jamesport Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Jamesport Brewing"
                }
            ]);
    };
    
    self.jadenJames = function () {
        self.logo("images/jadenjames.jpeg");
        self.details("Jaden James");
        self.vendorName("Jaden James Brewery");
        self.vendorNameOther("");
        self.vendorLocation("Grand Rapids, MI");
        self.showButton(true);
        self.isActive("jadenJames");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "xxxxxxxx"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "xxxxxxxx"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "xxxxxxxx"
                }
            ]);
    };
    
    self.jollyPumpkin = function () {
        self.logo("images/jollyPumpkin.jpeg");
        self.details("Jolly Pumpkin");
        self.vendorName("Jolly Pumpkin");
        self.vendorNameOther("");
        self.vendorLocation("Dexter, MI");
        self.showButton(true);
        self.isActive("jollyPumpkin");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Jolly Pumpkin"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Jolly Pumpkin"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Jolly Pumpkin"
                }
            ]);
    };
      
    self.loveWines = function () {
        self.logo("images/loveWines.jpg");
        self.details("Love Wines");
        self.vendorName("Love Wines");
        self.vendorNameOther("");
        self.vendorLocation("Ludington, MI");
        self.showButton(true);
        self.isActive("loveWines");
        self.menuList([{
                name: "Dog Days Semi-Sweet Watermelon",
                type: "Wine",
                abv: "5",
                vendor: "Love Wines"
                },
            {
                name: "Wine 2",
                type: "Wine",
                abv: "5",
                vendor: "Love Wines"
                },
            {
                name: "Wine 3",
                type: "Wine",
                abv: "5",
                vendor: "Love Wines"
                }
            ]);
    };
    
    self.ludingtonBay = function () {
        self.logo("images/ludingtonBay.jpeg");
        self.details("Ludington Bay");
        self.vendorName("Ludington Bay Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Ludington, MI");
        self.showButton(true);
        self.isActive("ludingtonBay");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Ludington Bay Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Ludington Bay Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Ludington Bay Brewing"
                }
            ]);
    };
    
    self.ludingtonBeverage = function () {
        self.logo("images/beerMug.jpeg");
        self.details("Ludington Beverage");
        self.vendorName("Ludington Beverage");
        self.vendorNameOther("");
        self.vendorLocation("Ludington, MI");
        self.showButton(true);
        self.isActive("ludingtonBeverage");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Ludington Beverage"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Ludington Beverage"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Ludington Beverage"
                }
            ]);
    };
    
    self.mitten = function () {
        self.logo("images/mitten.jpeg");
        self.details("Mitten");
        self.vendorName("Mitten Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Grand Rapids, MI");
        self.showButton(true);
        self.isActive("mitten");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Mitten Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Mitten Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Mitten Brewing"
                }
            ]);
    };
    
    self.mountainTown = function () {
        self.logo("images/mountainTown.jpeg");
        self.details("Mountain Town");
        self.vendorName("Mountain Town Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Mount Pleasant, MI");
        self.showButton(true);
        self.isActive("mountainTown");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Mountain Town Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Mountain Town Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Mountain Town Brewing"
                }
            ]);
    };
    
    self.newBelgium = function () {
        self.logo("images/newBelgium.jpeg");
        self.details("New Belgium");
        self.vendorName("New Belgium");
        self.vendorNameOther("");
        self.vendorLocation("Fort Collins, CO");
        self.showButton(true);
        self.isActive("newBelgium");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "New Belgium"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "New Belgium"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "New Belgium"
                }
            ]);
    };
    
        self.newHolland = function () {
        self.logo("images/newHolland.jpeg");
        self.details("New Holland");
        self.vendorName("New Holland");
        self.vendorNameOther("");
        self.vendorLocation("Holland, MI");
        self.showButton(true);
        self.isActive("newHolland");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "New Holland"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "New Holland"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "New Holland"
                }
            ]);
    };
    
    self.newaygo = function () {
        self.logo("images/newaygo.jpeg");
        self.details("Newaygo");
        self.vendorName("Newaygo Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Newaygo, MI");
        self.showButton(true);
        self.isActive("newaygo");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Newaygo Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Newaygo Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Newaygo Brewing"
                }
            ]);
    };
    
        self.northPeak = function () {
        self.logo("images/northPeak.jpeg");
        self.details("North Peak");
        self.vendorName("North Peak Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Traverse City, MI");
        self.showButton(true);
        self.isActive("northPeak");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "North Peak Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "North Peak Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "North Peak Brewing"
                }
            ]);
    };
    
        self.northernOak = function () {
        self.logo("images/northernOak.jpeg");
        self.details("Northern Oak");
        self.vendorName("Northern Oak");
        self.vendorNameOther("");
        self.vendorLocation("Holly, MI");
        self.showButton(true);
        self.isActive("northernOak");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Northern Oak"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Northern Oak"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Northern Oak"
                }
            ]);
    };
    
        self.ozones = function () {
        self.logo("images/ozones.jpeg");
        self.details("Ozones");
        self.vendorName("Ozone's Brewhouse");
        self.vendorNameOther("");
        self.vendorLocation("Lansing, MI");
        self.showButton(true);
        self.isActive("ozones");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Ozone's Brewhouse"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Ozone's Brewhouse"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Ozone's Brewhouse"
                }
            ]);
    };
    
        self.perrin = function () {
        self.logo("images/perrin.jpeg");
        self.details("Perrin");
        self.vendorName("Perrin Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Comstock Park, MI");
        self.showButton(true);
        self.isActive("perrin");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Perrin Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Perrin Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Perrin Brewing"
                }
            ]);
    };
    
        self.pigeonHill = function () {
        self.logo("images/pigeonHill.jpeg");
        self.details("Pigeon Hill");
        self.vendorName("Pigeon Hill Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Muskegon, MI");
        self.showButton(true);
        self.isActive("pigeonHill");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Pigeon Hill Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Pigeon Hill Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Pigeon Hill Brewing"
                }
            ]);
    };
    
        self.ridgeCider = function () {
        self.logo("images/ridgeCider.jpeg");
        self.details("Ridge Cider");
        self.vendorName("Ridge Cider Company");
        self.vendorNameOther("");
        self.vendorLocation("Grant, MI");
        self.showButton(true);
        self.isActive("ridgeCider");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Ridge Cider Company"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Ridge Cider Company"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Ridge Cider Company"
                }
            ]);
    };
    
        self.roak = function () {
        self.logo("images/roak.jpeg");
        self.details("Roak");
        self.vendorName("Roak Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Royal Oak, MI");
        self.showButton(true);
        self.isActive("roak");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Roak Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Roak Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Roak Brewing"
                }
            ]);
    };
    
        self.rochesterMills = function () {
        self.logo("images/rochesterMills.jpeg");
        self.details("Rochester Mills");
        self.vendorName("Rochester Mills");
        self.vendorNameOther("");
        self.vendorLocation("Rochester, MI");
        self.showButton(true);
        self.isActive("rochesterMills");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Rochester Mills"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Rochester Mills"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Rochester Mills"
                }
            ]);
    };
    
        self.samAdams = function () {
        self.logo("images/samAdams.jpeg");
        self.details("Sam Adams");
        self.vendorName("Sam Adams");
        self.vendorNameOther("");
        self.vendorLocation("Boston, MA");
        self.showButton(true);
        self.isActive("samAdams");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Sam Adams"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Sam Adams"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Sam Adams"
                }
            ]);
    };
    
    self.saugatuck = function () {
        self.logo("images/saugatuck.jpeg");
        self.details("saugatuck");
        self.vendorName("Saugatuck Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Douglas, MI");
        self.showButton(true);
        self.isActive("saugatuck");
        self.menuList([
            {
                name: "ESB",
                type: "Extra Special Bitter",
                abv: "5",
                vendor: "Saugatuck Brewing"
            },
            {
                name: "Lager",
                type: "Lager",
                abv: "5",
                vendor: "Saugatuck Brewing"
            },
            {
                name: "Starcut Cider",
                type: "Cider",
                abv: "5",
                vendor: "Saugatuck Brewing"
            }
        ]);
    };
    
        self.shorts = function () {
        self.logo("images/shorts.jpeg");
        self.details("Shorts");
        self.vendorName("Shorts Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Bellaire, MI");
        self.showButton(true);
        self.isActive("shorts");
        self.menuList([
            {
                name: "ESB",
                type: "Extra Special Bitter",
                abv: "5",
                vendor: "Shorts Brewing"
            },
            {
                name: "Lager",
                type: "Lager",
                abv: "5",
                vendor: "Shorts Brewing"
            },
            {
                name: "Starcut Cider",
                type: "Cider",
                abv: "5",
                vendor: "Shorts Brewing"
            }
        ]);
    };

    self.stAmbrose = function () {
        self.logo("images/stAmbrose.jpeg");
        self.details("St Ambrose");
        self.vendorName("St. Ambrose Cellars");
        self.vendorNameOther("");
        self.vendorLocation("Beulah, MI");
        self.showButton(true);
        self.isActive("stAmbrose");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "St. Ambrose Cellars"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "St. Ambrose Cellars"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "St. Ambrose Cellars"
                }
            ]);
    };
 
    self.starcut = function () {
        self.logo("images/starcut.jpeg");
        self.details("Starcut");
        self.vendorName("Starcut Ciders");
        self.vendorNameOther("");
        self.vendorLocation("Bellaire, MI");
        self.showButton(true);
        self.isActive("starcut");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Starcut Ciders"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Starcut Ciders"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Starcut Ciders"
                }
            ]);
    };    
    
    self.starvingArtist = function () {
        self.logo("images/starvingArtist.jpeg");
        self.details("Starving Artist");
        self.vendorName("Starving Artist");
        self.vendorNameOther("");
        self.vendorLocation("Ludington, MI");
        self.showButton(true);
        self.isActive("starvingArtist");
        self.menuList([{
                name: "Conspiracy",
                type: "IPA",
                abv: "5",
                vendor: "Starving Artist"
                },
            {
                name: "Drink 2",
                type: "Stout",
                abv: "5",
                vendor: "Starving Artist"
                },
            {
                name: "Drink 3",
                type: "Double IPA",
                abv: "5",
                vendor: "Starving Artist"
                }
            ]);
    };
    
    self.stormcloud = function () {
        self.logo("images/stormcloud.jpeg");
        self.details("Stormcloud");
        self.vendorName("Stormcloud Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Frankfort, MI");
        self.showButton(true);
        self.isActive("stormcloud");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Stormcloud Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Stormcloud Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Stormcloud Brewing"
                }
            ]);
    };
    
    self.trailPoint = function () {
        self.logo("images/trailPoint.jpeg");
        self.details("Trail Point");
        self.vendorName("Trail Point Brewing");
        self.vendorNameOther("");
        self.vendorLocation("Allendale, MI");
        self.showButton(true);
        self.isActive("trailPoint");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Trail Point Brewing"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Trail Point Brewing"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Trail Point Brewing"
                }
            ]);
    };
    
    self.traveler = function () {
        self.logo("images/traveler.jpeg");
        self.details("Traveler");
        self.vendorName("Traveler Beer Co");
        self.vendorNameOther("");
        self.vendorLocation("Burlington, VT");
        self.showButton(true);
        self.isActive("traveler");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Traveler Beer Co"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Traveler Beer Co"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Traveler Beer Co"
                }
            ]);
    };
    
    self.trulySpiked = function () {
        self.logo("images/trulySpiked.jpg");
        self.details("Truly Spiked");
        self.vendorName("Truly Spiked & Sparkling");
        self.vendorNameOther("");
        self.vendorLocation("Boston, MA");
        self.showButton(true);
        self.isActive("trulySpiked");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Truly Spiked"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Truly Spiked"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Truly Spiked"
                }
            ]);
    };

    self.vanderMill = function () {
        self.logo("images/vanderMill.jpeg");
        self.details("Vander Mill");
        self.vendorName("Vander Mill Cidery");
        self.vendorNameOther("");
        self.vendorLocation("Spring Lake, MI");
        self.showButton(true);
        self.isActive("vanderMill");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Vander Mill Cidery"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Vander Mill Cidery"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Vander Mill Cidery"
                }
            ]);
    };

    self.virtue = function () {
        self.logo("images/virtue.jpeg");
        self.details("Virtue");
        self.vendorName("Virtue Cider");
        self.vendorNameOther("");
        self.vendorLocation("Fennville, MI");
        self.showButton(true);
        self.isActive("virtue");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Virtue Cider"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Virtue Cider"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Virtue Cider"
                }
            ]);
    };

    self.walldorff = function () {
        self.logo("images/walldorff.jpeg");
        self.details("Walldorff");
        self.vendorName("Walldorff Brewpub");
        self.vendorNameOther("");
        self.vendorLocation("Hastings, MI");
        self.showButton(true);
        self.isActive("walldorff");
        self.menuList([{
                name: "Porter Name",
                type: "Porter",
                abv: "5",
                vendor: "Walldorff Brewpub"
                },
            {
                name: "Pilsner Name",
                type: "Pilsner",
                abv: "5",
                vendor: "Walldorff Brewpub"
                },
            {
                name: "Ale Name",
                type: "Golden Ale",
                abv: "5",
                vendor: "Walldorff Brewpub"
                }
            ]);
    };

    // vendor list
    self.vendorList([    
                     {  name: "Angry Orchard", 
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#58").click()
                        }                         
                     },                 
                     {  name: "Arbor Brewing", 
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#1").click()
                        }                         
                     }, 
                     {  name: "Arcadia Ales",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#2").click()
                        }                         
                     },                    
                     {name: "Atwater Brewery",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#3").click()
                        }                         
                     },
                     {name: "Austin Brothers",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#4").click()
                        }                         
                     },  
                     {name: "Backwoods Homemade Wine",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#5").click()
                        }                         
                     },  
                     {name: "Bell's Brewery",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#6").click()
                        }                         
                     },  
                     {name: "Big Hart Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#7").click()
                        }                         
                     },  
                     {name: "Big Lake Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#8").click()
                        }                         
                     },  
                     {name: "Blakes Hard Cider",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#9").click()
                        }                         
                     },  
                     {name: "Brewery Terra Firma",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#10").click()
                        }                         
                     },                      
                     {name: "Brewery Vivant",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#11").click()
                        }                         
                     },  
                     {name: "Cascade Winery",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#12").click()
                        }                         
                     },                    
                     {name: "Cheboygan Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#13").click()
                        }                         
                     },
                     {name: "Clown Shoes",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#14").click()
                        }                         
                     }, 
                     {name: "Coney Island Beer Co",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#60").click()
                        }                         
                     },  
                     {name: "Cranker's Brewery",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#15").click()
                        }                         
                     },  
                     {name: "Dark Horse Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#16").click()
                        }                         
                     },  
                     {name: "Dr Strange Love",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#17").click()
                        }                         
                     }, 
                     {name: "Elk Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#18").click()
                        }                         
                     },  
                     {name: "Ellison Brewery",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#19").click()
                        }                         
                     }, 
                     {name: "Farmhaus Cider Co",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#20").click()
                        }                         
                     },  
                     {name: "Fetch Brewing Co",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#21").click()
                        }                         
                     },  
                     {name: "Founders Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#22").click()
                        }                         
                     },  
                     {name: "Gravel Bottom Craft Brewery",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#23").click()
                        }                         
                     },  
                     {name: "Great Lakes Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#24").click()
                        }                         
                     },  
                     {name: "Great Lakes Wine & Spirits",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#25").click()
                        }                         
                     },  
                     {name: "Great Mead Hall",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#26").click()
                        }                         
                     }, 
                     {name: "Henry Fox Sales",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#27").click()
                        }                         
                     },  
                     {name: "Jamesport Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#28").click()
                        }                         
                     },  
                     {name: "Jaden James Brewery",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#59").click()
                        }                         
                     },  
                     {name: "Jolly Pumpkin Artisan Ales",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#29").click()
                        }                         
                     },  
                     {name: "Love Wines Winery of Ludington",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#30").click()
                        }                         
                     },  
                     {name: "Ludington Bay Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#31").click()
                        }                         
                     },  
                     {name: "Ludington Beverage",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#32").click()
                        }                         
                     },  
                     {name: "Mitten Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#52").click()
                        }                         
                     },  
                     {name: "Mountain Town Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#33").click()
                        }                         
                     },  
                     {name: "New Belgium Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#34").click()
                        }                         
                     },  
                     {name: "New Holland Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#35").click()
                        }                         
                     },  
                     {name: "Newaygo Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#36").click()
                        }                         
                     },  
                     {name: "North Peak Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#37").click()
                        }                         
                     },  
                     {name: "Northern Oak Brewery",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#38").click()
                        }                         
                     },  
                     {name: "Ozone's Brewhouse",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#39").click()
                        }                         
                     },  
                     {name: "Perrin Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#40").click()
                        }                         
                     },  
                     {name: "Pigeon Hill Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#41").click()
                        }                         
                     },  
                     {name: "Ridge Cider Company",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#42").click()
                        }                         
                     },  
                     {name: "Roak Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#43").click()
                        }                         
                     },  
                     {name: "Rochester Mills",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#44").click()
                        }                         
                     },  
                     {name: "Sam Adams",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#45").click()
                        }                         
                     },                    
                     {name: "Saugatuck Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#46").click()
                        }                         
                     },  
                     {name: "Short's Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#47").click()
                        }                         
                     },  
                     {name: "St. Ambrose",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#48").click()
                        }                         
                     }, 
                     {name: "Starcut Ciders",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#49").click()
                        }                         
                     }, 
                     {name: "Starving Artist",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#50").click()
                        }                         
                     },  
                     {name: "Stormcloud Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#51").click()
                        }                         
                     },  
                     {name: "Trail Point Brewing",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#53").click()
                        }                         
                     },  
                     {name: "Traveler Beer Co",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#54").click()
                        }                         
                     },                       
                     {name: "Vander Mill Cider",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#55").click()
                        }                         
                     },  
                     {name: "Virtue Cider",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#56").click()
                        }                         
                     },  
                     {name: "Walldorff Brewpub",                         
                        link: function() {
                            $("#vendorModal").modal("hide") 
                            $("#57").click()
                        }                         
                     }
      
                    ]);
    
    
    self.drinkList([    
                     {  name: "Mosaic Promise", 
                        type: "IPA",
                        vendor: "by Founders",
                        abv: "xxxxx",
                        goToVendor: function() {
                            $("#drinkModal").modal("hide") 
                            $("#22").click()
                        }                      
                     },                 
                     {  name: "Conspiracy", 
                        type: "IPA",
                        vendor: "by Starving Artist",
                        abv: "xxxxx",
                        goToVendor: function() {
                            $("#drinkModal").modal("hide") 
                            $("#50").click()
                        }                         
                     },                 
                     {  name: "Dog Days Semi-Sweet Watermelon", 
                        type: "Wine",
                        vendor: "by Love Wine",
                        abv: "xxxxx",
                        goToVendor: function() {
                            $("#drinkModal").modal("hide") 
                            $("#30").click()
                        }                         
                     }
    ]);
    


};

$('#removeAllBtn').on('click', function(){
    localStorage.removeItem('wishlist');
    $("#wishlistModal").modal("hide");
});

function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
        end = new Date().getTime();
    }
}   

var vm = new VendorViewModel();
ko.applyBindings(vm);